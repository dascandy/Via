#pragma once

#include <vector>
#include <cstdint>
#include <string>
#include "manto/udp_socket.hpp"
#include "bini/reader.hpp"
#include "bini/writer.hpp"

namespace Via {

extern network_address currentDnsServer;

// RFC 1035, page 12, plus wikipedia for AAAA / CAA
enum class RecordType : uint16_t {
  A = 1,
  AAAA = 28,
  ANY = 255,
  CAA = 257,
  CNAME = 5,
  MX = 15,
  NS = 2,
  OPT = 41,
  PTR = 12,
  SOA = 6,
  TXT = 16,
};

// For some weird reason requests and replies are the same type
struct Message {
  enum class Opcode : uint8_t {
    Query = 0,
    IQuery = 1,
    Status = 2,
  };
  enum class ResponseCode : uint8_t {
    NoError = 0,
    FormatError = 1,
    ServerFailure = 2,
    NameError = 3,
    NotImplemented = 4,
    Refused = 5,
  };

  struct Query {
    std::string name;
    RecordType type;
  };

  struct RR {
    std::string name;
    RecordType type;
    uint32_t ttl;
    std::string rdata;
    uint16_t net_class = 1;
  };

  inline bool isAnswer() { return flags & 0x8000; }
  inline Opcode opcode() { return (Opcode)((flags >> 11) & 0xF); }
  inline bool isAuthoritative() { return flags & 0x400; }
  inline bool isTruncated() { return flags & 0x200; }
  inline bool isRecursionDesired() { return flags & 0x100; }
  inline bool isRecursionAvailable() { return flags & 0x80; }
  inline ResponseCode getResponseCode() { return (ResponseCode)(flags & 0xF); }
  inline bool isCheckingDisabled() { return flags & 0x10; }
  inline bool isAuthenticatedData() { return flags & 0x20; }

  inline void setAnswer(bool v) { flags = (flags & 0x7FFF) | (v ? 0x8000 : 0x0); }
  inline void setOpcode(Opcode op) { flags = (flags & 0x87FF) | ((uint8_t)op << 11); }
  inline void setAuthoritative(bool v) { flags = (flags & 0xFBFF) | (v ? 0x400 : 0x0); }
  inline void setTruncated(bool v) { flags = (flags & 0xFDFF) | (v ? 0x200 : 0x0); }
  inline void setRecursionDesired(bool v) { flags = (flags & 0xFEFF) | (v ? 0x100 : 0x0); }
  inline void setRecursionAvailable(bool v) { flags = (flags & 0xFF7F) | (v ? 0x80 : 0x0); }
  inline void setResponseCode(ResponseCode rc) { flags = (flags & 0xFFF0) | ((uint8_t)rc); }
  inline void setCheckingDisabled(bool v) { flags = (flags & 0xFFEF) | (v ? 0x10 : 0x0); }
  inline void setAuthenticatedData(bool v) { flags = (flags & 0xFFDF) | (v ? 0x20 : 0x0); }

  std::vector<Query> queries;
  std::vector<RR> answers;
  std::vector<RR> authority;
  std::vector<RR> additional;

  std::vector<uint8_t> as_bytes();
  static Message from_bytes(const std::span<const uint8_t> bytes);
  uint16_t id = 0;
private:
  uint16_t flags = 0;
};

}


