#pragma once

#include "manto/udp_socket.hpp"
#include "manto/network_address.hpp"
#include "origo/time.hpp"

namespace Via {

// TransIP NTP server from pool.ntp.org
static network_address defaultNtp("5.39.184.5:123");

Origo::future<Origo::time> currentTimeNtp(network_address ntpServer = defaultNtp);
Origo::future<Origo::time> currentTime();

}


