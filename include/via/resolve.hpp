#pragma once

#include <manto/network_address.hpp>
#include <origo/future.hpp>

namespace Via {

Origo::future<std::vector<network_address>> resolve(std::string_view name, uint16_t port);

}


