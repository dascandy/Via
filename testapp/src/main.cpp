#include "via/resolve.hpp"
#include "manto/network_address.hpp"
#include <vector>

Origo::future<void> co_main() {
  for (auto addr : co_await Via::resolve("google.com", 443)) {
    printf("%s\n", to_string(addr).c_str());
  }
}

int main() {
  auto f = co_main();
}


