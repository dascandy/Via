#include "via/ntp.hpp"

#include <cstdint>
#include "bini/reader.hpp"
#include "bini/writer.hpp"
#include <chrono>

namespace Via {

static uint64_t parseReply(std::span<const uint8_t> message, uint64_t sendTime, uint64_t currentTime) {
  if (message.size() < 48) return 0;
  Bini::reader r(message);
  r.skip(24);
  uint64_t originT = r.read64be();
  if (originT != sendTime) return 0; // invalid message
  uint64_t recT = r.read64be();
  uint64_t sendT = r.read64be();
  return ((recT + sendT) - (sendTime + currentTime)) / 2;
}

static std::vector<uint8_t> createSntpRequest(uint64_t currentTime) {
  Bini::writer w;
  w.add8(0x23); // SNTP v4, client request
  w.add8(0); // stratum
  w.add8(0); // poll interval
  w.add8(0); // precision
  w.add32le(0); // root delay
  w.add32le(0); // root dispersion
  w.add32le(0); // Reference ID
  w.add64le(0); // reference timestamp
  w.add64le(0); // origin timestamp
  w.add64le(0); // receive timestamp;
  w.add64le(currentTime); // transmit timestamp
  return std::move(w);
}

Origo::future<Origo::time> currentTimeNtp(network_address ntpServer) {
  // TODO: timeout
  udp_socket ntp_client;
  co_await ntp_client.sendmsg(ntpServer, createSntpRequest(42));
  while (true) {
    auto pair = co_await ntp_client.recvmsg(576);
    auto [target, message] = pair;
    if (to_string(target) == to_string(ntpServer)) {
      uint64_t t = parseReply(message, 42, 42);
      co_return Origo::time{ int64_t(t >> 32), uint64_t(t << 32), "UTC" };
    }
  }
}

Origo::future<Origo::time> currentTime() {
  auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());
  uint64_t val = time.count() + 2'208'988'800'000'000'000;
  co_return Origo::time{ int64_t(val / 1000000000), (val % 1000000000) * 18446744073, "UTC" };
}

}


