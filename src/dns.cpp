#include "via/dns.hpp"
#include <string>
#include <map>
#include <vector>
#include "bini/writer.hpp"
#include "bini/reader.hpp"

struct dnswriter : Bini::writer {
  std::map<std::string, size_t> stringOffsets;
  void addDnsString(const std::string& s) {
    if (auto it = stringOffsets.find(s); it != stringOffsets.end()) {
      add16be(0xC000 | it->second);
    } else if (size_t offset = s.find_first_of('.'); offset != std::string::npos) {
      add8(offset);
      add(s.substr(0, offset));
      addDnsString(s.substr(offset+1));
    } else if (s.empty()) {
      add8(0);
    } else {
      add8(s.size());
      add(s);
      add8(0);
    }
  }
  void addQuery(Via::Message::Query& q) {
    addDnsString(q.name);
    add16be((uint16_t)q.type);
    add16be(1); // Internet. 
  }
  void addRR(Via::Message::RR& r) {
    addDnsString(r.name);
    add16be((uint16_t)r.type);
    add16be(r.net_class);
    add32be(r.ttl);
    add16be((uint16_t)r.rdata.size());
    add(r.rdata);
    if (r.rdata.size() % 2) add8(0);
  }
};

struct dnsreader : Bini::reader {
  dnsreader(const std::span<const uint8_t> bytes)
  : reader(bytes)
  {}
  std::map<size_t, std::string> stringOffsets;
  std::string readDnsString() {
    uint8_t v = read8();
    if ((v & 0xC0) == 0xC0) {
      // Strictly speaking, we should now look at another point in the data stream.
      uint16_t off = ((v << 8) | read8()) & 0x3FFF;
      if (auto it = stringOffsets.find(off); it != stringOffsets.end()) {
        stringOffsets[p - b - 1] = it->second;
        return it->second;
      }
      setFail();
      return "";
    } else if (v == 0) {
      return "";
    } else {
      size_t offset = p - b - 1;
      auto sp = get(v & 0x3F);
      std::string partial = std::string(sp.begin(), sp.end());
      std::string remainder = readDnsString();
      if (!remainder.empty()) partial += "." + remainder;
      stringOffsets[offset] = partial;
      return partial;
    }
  }
  Via::Message::Query readQuery() {
    Via::Message::Query q;
    q.name = readDnsString();
    q.type = (Via::RecordType)(read16be());
    if (read16be() != 1) setFail();
    return q;
  }
  Via::Message::RR readRR() {
    Via::Message::RR r;
    r.name = readDnsString();
    r.type = (Via::RecordType)(read16be());
    r.net_class = read16be();
    r.ttl = read32be();
    size_t rdatasize = read16be();
    auto s = get(rdatasize);
    r.rdata = std::string(s.begin(), s.end());
    return r;
  }
};

std::vector<uint8_t> Via::Message::as_bytes() {
  dnswriter w;
  w.add16be(id);
  w.add16be(flags);
  w.add16be(queries.size());
  w.add16be(answers.size());
  w.add16be(authority.size());
  w.add16be(additional.size());

  for (auto& q : queries) w.addQuery(q);
  for (auto& r : answers) w.addRR(r);
  for (auto& r : authority) w.addRR(r);
  for (auto& r : additional) w.addRR(r);
  return std::move(w);
}

Via::Message Via::Message::from_bytes(const std::span<const uint8_t> bytes) {
  dnsreader r(bytes);
  Via::Message m;
  m.id = r.read16be();
  m.flags = r.read16be();
  size_t queries_size = r.read16be();
  size_t answers_size = r.read16be();
  size_t authority_size = r.read16be();
  size_t additional_size = r.read16be();
  for (size_t n = 0; !r.fail() && n < queries_size; n++) m.queries.push_back(r.readQuery());
  for (size_t n = 0; !r.fail() && n < answers_size; n++) m.answers.push_back(r.readRR());
  for (size_t n = 0; !r.fail() && n < authority_size; n++) m.authority.push_back(r.readRR());
  for (size_t n = 0; !r.fail() && n < additional_size; n++) m.additional.push_back(r.readRR());
  return r.fail() ? Via::Message() : m;
}

std::vector<network_address> dnsRootServers = {
  { "2001:503:ba3e::2:30" },
  { "2001:500:200::b" },
  { "2001:500:2::c" },
  { "2001:500:2d::d" },
  { "2001:500:a8::e" },
  { "2001:500:2f::f" },
  { "2001:500:12::d0d" },
  { "2001:500:1::53" },
  { "2001:7fe::53" },
  { "2001:503:c27::2:30" },
  { "2001:7fd::1" },
  { "2001:500:9f::42" },
  { "2001:dc3::35" },
};

std::vector<network_address> defaultDnsServers = {
  { "1.1.1.1:53" },
// XS4All:
  { "[2001:888:0:6::66]:53" },
  { "[2001:888:0:9::99]:53" },

// Google:
  { "[2001:4860:4860::8888]:53" },
  { "[2001:4860:4860::8844]:53" },

// Cloudflare:
  { "[2606:4700:4700::1111]:53" },
  { "[2606:4700:4700::1001]:53" },

// Cisco:
  { "[2620:119:35::35]:53" },
  { "[2620:119:53::53]:53" },
};

network_address Via::currentDnsServer = defaultDnsServers[1];

