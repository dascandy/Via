#include "via/resolve.hpp"
#include "via/dns.hpp"

namespace Via {

inline std::vector<uint8_t> createDnsRequest(std::string_view name, uint16_t requestId, RecordType recordType) {
  Message m;
  m.queries.push_back({std::string(name), recordType});
  m.additional.push_back({"", RecordType::OPT, 0, {0x00, 0x0a, 0x00, 0x08, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07}, 4096});
  m.id = requestId;
  m.setAnswer(false);
  m.setRecursionDesired(true);
  m.setCheckingDisabled(true);
  return m.as_bytes();
}

inline void parseReply(std::vector<uint8_t>& message, uint16_t port, std::vector<network_address>& addresses) {
  Message m = Message::from_bytes(message);
  if (!m.isAnswer()) return;
  for (auto& a : m.answers) {
    if (a.type == RecordType::A) {
      struct sockaddr_in addr;
      memcpy(&addr.sin_addr.s_addr, a.rdata.data(), a.rdata.size());
      addr.sin_port = htons(port);
      addr.sin_family = AF_INET;
      addresses.push_back(network_address((struct sockaddr*)&addr, sizeof(addr)));
    } else if (a.type == RecordType::AAAA) {
      struct sockaddr_in6 addr;
      memcpy(&addr.sin6_addr.s6_addr, a.rdata.data(), a.rdata.size());
      addr.sin6_port = htons(port);
      addr.sin6_family = AF_INET6;
      addresses.push_back(network_address((struct sockaddr*)&addr, sizeof(addr)));
    }
  }
}

Origo::future<std::vector<network_address>> resolve(std::string_view name, uint16_t port) {
  udp_socket dns_client = udp_socket();
  network_address dns_server = currentDnsServer;
  std::vector<network_address> addresses;
  co_await dns_client.sendmsg(dns_server, createDnsRequest(name, 0x4242, RecordType::AAAA));
  co_await dns_client.sendmsg(dns_server, createDnsRequest(name, 0x4243, RecordType::A));
  for (size_t n = 0; n < 2; n++) {
    auto [target, message] = co_await dns_client.recvmsg(576);
    if (to_string(target) == to_string(dns_server)) {
      parseReply(message, port, addresses);
    }
  }
  co_return addresses;
}

}


